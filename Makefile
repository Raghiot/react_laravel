include ./.env
export

dev:
	@code .
	@echo "starting Dev evnviroment"
	@cd frontend && npm start &
	@cd backend && php artisan serve
install:
	@echo "install lib"
	@cd backend && composer install && cp .env.example .env
	@cd frontend && npm install

freshdb:
	@echo 'refresh db'
	@cd backend && php artisan migrate:fresh && php artisan passport:install --force