import './App.css';
import Home from './Components/Views/Home/Home';
import Navbar from './Components/UI/Navbar/Navbar';
import Footer from './Components/UI/Footer/Footer';
import Game from './Components/Views/Game/Game';
import Search from './Components/Views/Search/Search';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import { AuthProvider } from "./Contexts/Auth"
import { ConfigProvider } from "./Contexts/Config"
import Sign from './Components/Views/Sign/Sign';
import Profile from './Components/Views/Profile/Profile';
import Stream from './Components/Views/Stream/Stream';

//utilities
import ProtectedRoute from './Components/Utilities/ProtectedRoute';
function App() {
  return (
    <ConfigProvider>
      <AuthProvider>
      <Router>
        <Navbar />


        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/search/:genre/:num" element={<Search />} />
          <Route path="/Game/:slug" element={<Game />} />
          <Route path="/login" element={<Sign />} />
          <Route path="/profile" element={<Profile />} />
          
          <Route exact path="/stream/:game_name/:game_id" element={<ProtectedRoute />}/>
          
          



        </Routes>
        <Footer />
      </Router>
      </AuthProvider>
    </ConfigProvider>
  );
}

export default App;
