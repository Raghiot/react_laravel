import classes from "./Header.module.css";
import Video from "./../../../Assets/video.mkv";
import Card from "../Card/Card";

export default function Header() {
    return (

        <header>


            <div className={classes.overlay}>


            </div>


            <video
                playsInline
                autoPlay
                muted="muted"
                
            >
                <source src={Video} type="video/mp4" />
            </video>


            <div className={classes.container + " h-100"} >
                <div className="d-flex h-100 text-center align-items-center">
                    <div className="w-100 myColor">
                        <h1 className="display-3">Rehacktor</h1>
                        
                        <p className="lead mb-0 myColor"><button className="btn myColor">Explore</button></p>
                    </div>
                </div>
            </div>
            
        </header>
        

    );

}