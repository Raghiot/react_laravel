import {useEffect, useState} from 'react'
import {Link} from "react-router-dom"
import Card from '../Card/Card'


export default function Featured(){
    const [featured,setFeatured] = useState(null)

    useEffect(() =>{
        fetch('https://api.rawg.io/api/games?key=4062ee0fbf154a198acd79c9e1963763&dates=2021-09-01,2021-09-30&platforms=18,1,7')
        .then((response) => response.json())
        .then((data) => {
            
            setFeatured(data.results.slice(0,4))
        
        })

    },[])
    
    return (
        <div className="container mt-5 py-5">
            <div className='row'>
                {featured && 
                    featured.map((el)=>{
                        return (
                            <div key={el.id} className="col-12 col-md-6 col-lg-3 mb-5">
                                <Card image={el.background_image} name={el.name} slug={el.slug}/>
                                
                               
                            </div>
                        )
                    })

                }
            </div>
        </div>


    )
    
}