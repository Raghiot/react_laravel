import classes from "./Footer.module.css"


export default function Footer(){
	console.log(classes);
    return(
		
        <footer className={classes.myTop + "myColor myH shadow py-1 w-100 bg-transparent border-top"} >
    
    <div className="container-fluid d-none d-xl-block">
        <div className="row justify-content-start">
            <div className="col-12 col-md-2 py-1">
                <ul className="navbar-nav">
                <li className="myFontTitleFooter">CLIENT SERVICE</li>
                <li className="myFontFooter pt-1"> <a href="" className="myFontFooter myColor"> CONTACT</a></li>
                <li className="myFontFooter myColor pt-0"> <a href="" className="myFontFooter myColor"> SALE POINT</a></li>
                <li></li>
            </ul>
        </div>
        <div className="col-12 col-md-2 py-1">
            <ul className="navbar-nav">
                <li className="myFontTitleFooter">ONLINE SERVICE</li>
                <li className="myFontFooter myColor pt-1"> <a href="" className="myFontFooter myColor"> PAYMENT AND SECURITY</a></li>
                <li className="myFontFooter myColor pt-0"> <a href="" className="myFontFooter myColor"> RETURN AND REFUND</a></li>
                <li className="myFontFooter myColor pt-0"> <a href="" className="myFontFooter myColor"> REPAIR POLICY</a></li>
            </ul>
        </div>
        <div className="col-12 col-md-2 py-1">
            <ul className="navbar-nav">
                <li className="myFontTitleFooter">LEGAL INFO</li>
                <li className="myFontFooter myColor pt-1"><a href="" className="myFontFooter myColor"> PRIVACY POLICY</a></li>
                <li className="myFontFooter myColor pt-0"><a href="" className="myFontFooter myColor"> COOKIE POLICY</a></li>
                <li className="myFontFooter myColor pt-0"><a href="" className="myFontFooter myColor"> TERMS  CONDITION</a></li>
                <li></li>
            </ul>
        </div>
        <div className="col-12 col-md-2 py-1">
            <ul className="navbar-nav">
                <li className="myFontTitleFooter">FOLLOW US</li>
                <li className="myFontFooter myColor pt-1"><i className="fab fa-facebook-square fa-xm"><a href="" className="pl-1 myFontFooter myColor"> FACEBOOK</a></i></li>
                <li className="myFontFooter myColor pt-0"><i className="fab fa-instagram fa-xm"><a href="" className="pl-1 myFontFooter myColor"> INSTAGRAM</a></i></li>
                <li className="myFontFooter myColor pt-0"><i className="fa-solid fa-square-envelope fa-xm"><a role="button" className=" pl-1 myFontFooter myColor" data-toggle="modal" data-target="#newsLetter">
                     NEWSLETTER
                  </a></i>
                    </li>
                
                <li></li>
            </ul>
        </div>
        <div className="col-12 col-md-4 d-flex align-items-end">
            <ul className="navbar-nav ml-auto">
                <li className="d-flex align-items-end"><p className="copyRight text-right pt-2 mr-2 mb-0">COPYRIGHT © 2022 PIO - ALL RIGHTS RESERVED</p></li>
                <li></li>
            </ul>
        </div>
    </div>
</div>






</footer>

    )


}