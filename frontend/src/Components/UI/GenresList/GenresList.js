import { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import classes from './GenresList.module.css'


export default function GenresList(props) {
    //console.log("props",props);

    return (
        <div className={classes["genres-wrapper"]}>
            {props.data.map((genre) => {
                return (
                <Link to={`/search/${genre.slug}/1`} className="text-decoration-none" key={genre.name}>
                <button  className='btn btn-outline-light rounded-0 d-block w-100 mb-2 text-start'>
                    {genre.name}
                </button>
                </Link>)
            })


            }
        </div>
    )
}