import { useState, useContext } from 'react'
import classes from './SignUp.module.css'
import { ConfigContext } from '../../../Contexts/Config'
import { AuthContext } from '../../../Contexts/Auth'
import useInput from './../../../Hooks/useInput'
import { useNavigate } from "react-router"


export default function SignUp() {
    const history = useNavigate()
    let { login } = useContext(AuthContext)
    let { api_urls } = useContext(ConfigContext)

    const email = useInput("")
    const password = useInput("")



    const signUp = (event) => {
        event.preventDefault()
        //console.log("ok");
        //login("Gino","ABC",123)
        fetch(`${api_urls.backend}/api/users/login`,
            {
                method: "POST",
                headers: { "Content-Type" : "application/json" },
                body: JSON.stringify({ email: email.value, password: password.value })

            })
            .then(response => response.json())
            .then(data => {
                const token = data.token
                fetch(`${api_urls.backend}/api/users/view-profile`,
                    {
                        method: "GET",
                        headers: { 
                            "Authorization": `Bearer ${token}`
                         },
                        

                    })
                    .then(response => response.json())
                    .then(data => {
                       
                        login(data.data.name, token, data.data.id)
                        history("/")//stato sotituito con navigate

                        //una volta ricevuto il token chiediamo name ed id
                    })


                //una volta ricevuto il token chiediamo name ed id
            })


    }


    return (
        <>
            <form className="sign-form" onSubmit={signUp}>
                <div className="sign-top"></div>
                <div className="sign-bottom"></div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userMail'>Inserisci mail</label>
                    <input {...email} className='form-control bg-transparent border-0 border-bottom border-light  rounded-0 text-white' type="email" id='userMail'></input>
                </div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userPassword'>Inserisci password</label>
                    <input {...password} className='form-control bg-transparent border-0 border-bottom border-light rounded-0 text-white' type="password" id='userPassword'></input>
                </div>
                <div className='mb-5'>
                    <button type='submit' className='btn btn-outline-light px-5'>Login</button>
                </div>
            </form>

        </>
    )
}