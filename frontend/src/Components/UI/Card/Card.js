import {useState, useEffect} from 'react'
import classes from './Card.module.css'
import { Link } from 'react-router-dom'


export default function Card(props){
    
    const [card,setCard] = useState(null)

    // useEffect(() => {
    //     fetch('https://api.rawg.io/api/games?key=4062ee0fbf154a198acd79c9e1963763&dates=2019-09-01,2019-09-30&platforms=18,1,7')
    //     .then(response => response.json())
    //     .then(data => {console.log(data.results);})


    // },[])
    
    
    return <div className={classes["card-game"]+" border my-5"}>
        <img src={props.image}alt='test'/>
        <p className='text-white'>{props.name}</p>
        <Link to={`/game/${props.slug}`}>
        <i className="fa-solid fa-chevron-right text-white"></i>
        </Link>
        <div></div>
        <div></div>


    </div>






}