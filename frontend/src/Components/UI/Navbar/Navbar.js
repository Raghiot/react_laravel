import { Link } from 'react-router-dom'
import { AuthContext } from '../../../Contexts/Auth'
import { useContext, useState } from 'react'
import Modal from '../Modal/Modal'
export default function Navbar() {
    const { user, logout } = useContext(AuthContext)
    const [modal, setModal] = useState(false)

    const closeModal = () => setModal(false)


    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-transparent fixed-top border-bottom">
            <div className="container-fluid">
                <Link className="navbar-brand myColor fw-bold" to="/">Navbar</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link active myColor" aria-current="page" to="/search/action/1">Search</Link>
                        </li>
                        {!user && <li className="nav-item">
                            <Link className="nav-link active myColor" aria-current="page" to="/login">Log in</Link>
                        </li>

                        }
                        {user && (
                            <>
                                {modal && <Modal
                                    title="Oh no..."
                                    message="Eventuali stream in corso saranno interrotti"
                                    declineMessage="Rimani"
                                    confirmMessage="Esci"
                                    closeModal={closeModal}
                                    action={logout}
                                />}
                                <li className="nav-item d-flex align-items-center" >
                                    <Link to="/profile" className='nav-link btn nav-link text-white '><i className="fa-solid fa-users me-2"></i>{user.username}</Link>
                                </li>
                                <li className="nav-item d-flex align-items-center" >
                                    <button className='btn nav-link btn nav-link text-white' onClick={() => setModal(true)}><i className="fa-solid fa-right-from-bracket"></i></button>
                                </li>
                            </>)}
                        {/* <li className="nav-item">
                            <a className="nav-link myColor" href="#">Link</a>
                        </li> */}


                    </ul>
                    {/* <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-success" type="submit">Search</button>
                    </form> */}
                </div>
            </div>
        </nav>




    )



}