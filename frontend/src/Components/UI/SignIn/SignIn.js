import { useState,useContext } from 'react'
import classes from './SignIn.module.css'
import { ConfigContext } from '../../../Contexts/Config'
import { AuthContext } from '../../../Contexts/Auth'
import useInput from './../../../Hooks/useInput'
import { useNavigate } from "react-router"

export default function SignIn() {
    const history = useNavigate()

    const username = useInput("")
    const email = useInput("")
    const password = useInput("")
    const passwordConfirm = useInput("")
    let {api_urls} = useContext(ConfigContext)
    let {login} = useContext(AuthContext)

    const signIn = (event) => {
        event.preventDefault()

        if(password.value === passwordConfirm.value){
            fetch(`${api_urls.backend}/api/users/register`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ 
                    name:username.value, 
                    email: email.value, 
                    password: password.value,
                    password_confirmation:passwordConfirm.value })

            })
            .then(response => {
                if(response.ok){
                    return response.json()

                }else{
                    alert("Bad request")
                }

            })
            .then(() => {
                fetch(`${api_urls.backend}/api/users/login`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ email: email.value, password: password.value })

            })
            .then(response => response.json())
            .then(data => {
                const token = data.token
                fetch(`${api_urls.backend}/api/users/view-profile`,
                    {
                        method: "GET",
                        headers: { 
                            "Authorization": `Bearer ${token}`
                         },
                        

                    })
                    .then(response => response.json())
                    .then(data => {
                       console.log("data login", data);
                        login(data.data.name, token, data.data.id)
                        history("/")//stato sotituito con navigate

                        //una volta ricevuto il token chiediamo name ed id
                    })



                //una volta ricevuto il token chiediamo name ed id
            })})



        } else{
            alert("le password non corrispondono")
        }


    }





    return (
        <>
            <form className="sign-form" onSubmit={signIn}>
                <div className="sign-top"></div>
                <div className="sign-bottom"></div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userName'>Inserisci Name</label>
                    <input {...username} className='form-control bg-transparent border-0 border-bottom border-light  rounded-0 text-white' type="text" id='userName'></input>
                </div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userMail'>Inserisci mail</label>
                    <input {...email} className='form-control bg-transparent border-0 border-bottom border-light  rounded-0 text-white' type="email" id='userMail'></input>
                </div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userPassword'>Inserisci password</label>
                    <input {...password} className='form-control bg-transparent border-0 border-bottom border-light rounded-0 text-white' type="password" id='userPassword'></input>
                </div>
                <div className='mb-5'>
                    <label className='form-label' htmlFor='userPasswordConfirm'>Inserisci nuovamente la password</label>
                    <input {...passwordConfirm} className='form-control bg-transparent border-0 border-bottom border-light rounded-0 text-white' type="password" id='userPasswordConfirm'></input>
                </div>
                <div className='mb-5'>
                    <button type='submit' className='btn btn-outline-light px-5'>Register</button>
                </div>
            </form>
            
        </>
    )
}