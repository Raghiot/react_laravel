import { Component, useContext } from "react";
import { Redirect, Route, useNavigate, Navigate, Outlet } from "react-router-dom";
import { AuthContext } from "../../Contexts/Auth";
import Stream from "../Views/Stream/Stream";

const ProtectedRoute = ({ component: Component, ...rest }) => {

    const { user } = useContext(AuthContext)

    return (
        
        // <Route
        //     {...rest}
        //     render={(props) => {
        //         if (user) {
        //             return <Component {...rest} {...props} />
        //         } else {
        //            return <Navigate replace to={"/"} />
        //         }
        //     }}
        // />
        <Outlet
            {...rest}
            render={(props) => {
                if (user) {
                    return <Component {...rest} {...props} />
                } else {
                   return <Navigate replace to={"/"} />
                }
            }}
        />
        
    )

}

export default ProtectedRoute;