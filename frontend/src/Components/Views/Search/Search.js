import { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import classes from './Search.module.css'
import GenresList from '../../UI/GenresList/GenresList'
import Card from '../../UI/Card/Card'
import { Link } from 'react-router-dom'


export default function Search() {
    const [games, setGames] = useState()
    const [genres, setGenres] = useState(null)
    const [searched, setSearched] = useState('')
    let { genre } = useParams()
    let { num } = useParams()
    useEffect(() => {
        fetch('https://api.rawg.io/api/genres?key=4062ee0fbf154a198acd79c9e1963763&')
            .then((response) => response.json())
            .then((data) => {

                //console.log(genre,"genre",num);
                setGenres(data.results)

            })

    }, [""])
    useEffect(() => {
        setGames(null)
        fetch(`https://api.rawg.io/api/games?key=4062ee0fbf154a198acd79c9e1963763&genres=${genre}&page=${num}&page_size=12`)
            .then((response) => response.json())
            .then((data) => {
                setGames(data.results)
                //console.log(data.results)

            })

    }, [genre, num])
    useEffect(() => {
        if (searched.length > 4)
            fetch(`https://api.rawg.io/api/games?key=4062ee0fbf154a198acd79c9e1963763&page_size=24&search=${searched}&search_precise=true`)
                .then((response) => response.json())
                .then((data) => {
                    console.log(data)
                    setGames(data.results)
                    //setSearched(data.results)

                })

    }, [searched])
    return (
        <div className='container-fluid my-5 py-5 min-vh-100'>
            <div className=' row my-5'>
                <div className='col-12 col-md-3 col-lg-2'>
                    {genres && <GenresList data={genres}>
                    </GenresList>}
                </div>
                <div className='col-12 col-md-9 col-lg-10'>
                    <div className='row'>
                        <div className='col-12 col-md-6 col-lg-4'>
                            <div className='input-group mb-3'>
                                <input
                                    type="text"
                                    className='form-control bg-transparent border-light rounded-0 text-white'
                                    placeholder='Search by name'
                                    onChange={(ev) => setSearched(ev.target.value)}
                                    value={searched} />
                                    {searched}
                                <button className='btn btn-outline-light' type='button'>Search</button>
                            </div>
                        </div>
                    </div>
                    {!searched && <div className='row justify-content-between mb-5'>
                        <div className='col-2'>
                            {num > 1 ? <Link className='text-decoration-none text-white ps-5' to={`/search/${genre}/${+num - 1}`}><i className="fa-solid fa-chevron-left text-white fa-2x"></i></Link> : ""}
                        </div>
                        <div className='col-2'>
                            <p className='text-white'>Page: {num}</p>
                        </div>
                        <div className='col-2'>
                            <Link className='text-decoration-none text-white pe-5' to={`/search/${genre}/${+num + 1}`}><i className="fa-solid fa-chevron-right text-white fa-2x"></i></Link>

                        </div>

                    </div>}
                    <div className='row'>
                        {games && games.map(game => {
                            return (<div key={game.id} className='col-12 col-md-6 col-lg-4'>
                                <Card
                                    key={game.id}
                                    image={game.background_image}
                                    name={game.name}
                                    slug={game.slug} />
                            </div>)
                        })

                        }
                    </div>
                </div>
            </div>

        </div>

    )
}