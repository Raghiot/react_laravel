import { useParams } from "react-router-dom"
import { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { ConfigContext } from "./../../../Contexts/Config"
import { AuthContext } from "./../../../Contexts/Auth"
export default function Game() {

    let { api_urls, api_secrets } = useContext(ConfigContext)
    let { user } = useContext(AuthContext)
    let { slug } = useParams()
    const [game, setGame] = useState(null)
    console.log(api_urls.games);

    useEffect(() => {
        fetch(`${api_urls.games}/api/games/${slug}?key=4062ee0fbf154a198acd79c9e1963763&`)
            .then((response) => response.json())
            .then((data) => {

                setGame(data)


            })


    }, [""])


    return (
        <>
            {game &&

                <div className="container-fluid pt-5 min-vh-100" style={{
                    background: `linear-gradient(rgba(33,33,33,1),rgba(33,33,33,0.5),rgba(33,33,33,1)), url(${game.background_image})`,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat"
                }}>
                    <div className="container">
                        <div className="row mt-5">
                            <div className="col-12">
                                <h1>{game.name}</h1>
                                <p className="small text-white">developed by {game.developers[0].name}</p>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-12 col-md-6">
                                <p className="small text-white">{game.description_raw}</p>
                            </div>


                            <div className="col-12 col-md-6">
                                <img className="img-fluid" src={game.background_image} alt={game.name} />
                            </div>
                        </div>
                        <div className="row mt-5">

                            <div className="col-6">
                                <h3>Genres</h3>
                                {game.genres.map((el) => {

                                    return (
                                        <Link key={el.id} className="text-white text-decoration-none" to={`/search/${el.slug}/1`}>

                                            <button className="btn btn-outline-light me-2">{el.name}</button>
                                        </Link>
                                    )
                                })

                                }
                            </div>
                            <div className="col-6">
                                
                                {user ? (
                                <Link to={`/stream/${game.slug}/${game.id}`} className="h3 text-white text-decoration-none fst-italic">
                                    <i className="fa-solid fa-chevron-right text-white pe-3">
                                        </i>Start your Stream
                                        </Link>) : (<Link to='/login' className="h3 text-white text-decoration-none fst-italic">
                                    <i className="fa-solid fa-chevron-right text-white pe-3">
                                        </i>Login
                                        </Link>)
                                }
                            </div>
                        </div>

                    </div>
                </div>

            }
        </>


    )


}