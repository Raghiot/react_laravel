import { useState } from 'react'
import SignIn from '../../UI/SignIn/SignIn'
import SignUp from '../../UI/SignUp/SignUp'
import classes from './Sign.module.css'

export default function Sign() {
    const [isLogin, SetIsLogin] = useState(true)

    return (
        <div className={'container-fluid min-vh-100 bg ' + classes.bg}>
            <div className='container'>
                <div className='row justify-content-center min-vh-100 bg-transparent align-items-center'>
                    <div className='col-12 col-md-8 col-lg-6'>
                        {isLogin ? <SignUp /> : <SignIn />}
                        <button className='mt-5 btn btn-outline-light' onClick={() => SetIsLogin(!isLogin)}>
                            {isLogin ? 'Not a user? Register now' : "Already a user? Log in now"}
                        </button>

                    </div>
                </div>
            </div>
        </div>

    )


}